#  BDPO PMPI Wrapper

How to install
--------------------------------

### Set an environment

    $ spack env create myenv # If needed
    $ spack env activate -p -v myenv

### Add wrappers (example for OpenMPI and IntelMPI)

    $ spack add bdpo-pmpi-wrapper-openmpi
    $ spack add bdpo-pmpi-wrapper-intelmpi

If you want to specify a specific version for the MPI compiler, 

    $ spack add bdpo-pmpi-wrapper-openmpi ^openmpi@4.1.2

### Install 

    $ spack install

How to find the generated artifacts
--------------------------------

### Using 'spack load':

If you want to use, for example, the openmpi wrapper, you just have to load it:

    $ spack load bdpo-pmpi-wrapper-openmpi

And then, an environment variable is available to find the generated library:

    $ echo $BDPOPMPIWRAPPER
    $SPACK_VIEW/opt/bdpo-pmpi-wrapper/bdpopmpiwrapper-<mpi_provider_name>-<mpi_provider_version>.so


### Manually find the library:

After a successfull installation of any bdpo-pmpi-wrapper-*, you can find the 
generated dynamic libraries here:

```bash
    $SPACK_VIEW/opt/bdpo-pmpi-wrapper/bdpopmpiwrapper-<mpi_provider_name>-<mpi_provider_version>.so
```

> The `$SPACK_VIEW` commonly refers to `$SPACK_ROOT/var/spack/environments/<env_name>/.spack-env/view`
> with `$SPACK_ROOT` the Spack installation root folder

Custom MPI Provider support
--------------------------------

You can add a Spack package card by copying any bdpo-pmpi-wrapper-* and edit the
MPI compiler, name and version.

Example:

```python
from spack import *

# Workaround for imported Python module with dashes
import importlib
bdpopmpiwrapper = importlib.import_module('spack.pkg.builtin.bdpo-pmpi-wrapper')

class BdpoPmpiWrapperIntelCustomMpi(bdpopmpiwrapper.BdpoPmpiWrapperPackage):
    """This package provides the 
       Power Efficiency Dynamic Power Optimizer PMPI Wrapper product for CustomMPI
    """

    depends_on("custom-mpi")

    def bdpo_get_mpi_env(self, spec):
        return {
            'mpicc': spec['mpi'].mpicc,
            'mpich_enabled': '1', # 0 if your MPI compiler supports Fortran bindings else 1
            'mpi_name': spec['mpi'].name,
            'mpi_version': spec['mpi'].version,
        }
```
