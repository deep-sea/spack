# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
# Copyright 2022-2023 Bull S.A.S - All rights reserved
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *
import os


class Parcoach(CMakePackage):
    """Analysis tool for MPI programs"""

    homepage = "https://parcoach.github.io/index.html"
    git      = "https://github.com/parcoach/parcoach.git"
    url      = "https://github.com/parcoach/parcoach/archive/refs/tags/2.4.0.tar.gz"

    version("2.4.0", sha256="7107be7fd2d814b5118d2213f5ceda48d24d3caf97c8feb19e7991e0ebaf2455")
    version("deepsea-v2", sha256="7107be7fd2d814b5118d2213f5ceda48d24d3caf97c8feb19e7991e0ebaf2455")

    depends_on('classicclang@15.0.3%gcc')
    depends_on('classicflang@15.0.3%gcc')
    depends_on('cmake@3.24%gcc', type='build')
    depends_on('openmpibull+classicclang-15')

    def cmake_args(self):
        cache_path = os.path.join(self.stage.source_path, "caches", "Release-shared.cmake")
        args = [
                # Preload cmake cache for the shared release
                f"-C {cache_path}",
                self.define("CMAKE_CXX_FLAGS", "-L/usr/local/software/skylake/Stages/2023/software/GCCcore/11.3.0/lib64 -lstdc++"),
                self.define("CMAKE_C_COMPILER", "clang"),
                self.define("CMAKE_CXX_COMPILER", "clang++"),
                self.define("CMAKE_Fortran_COMPILER", "flang"),
                self.define("CMAKE_Fortran_COMPILER_ID", "LLVMFlang"),
                ]
        return args

    #  def setup_build_environment(self, env):
    #      env.set('CC', 'clang')
    #      env.set('CXX', 'clang++')
    #      env.set('FC', 'flang')

    def setup_run_environment(self, env):
        # Ideally we would ask user to use CMake integration, and not "pollute"
        # these common flags. This is only useful for instrumentation.
        env.prepend_path("LD_LIBRARY_PATH", self.prefix.lib)
        env.prepend_path("LIBRARY_PATH", self.prefix.lib)
