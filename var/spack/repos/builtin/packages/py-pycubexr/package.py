# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
# Copyright (c) 2022, Technical University of Darmstadt, Germany
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyPycubexr(PythonPackage):
    """pyCubexR is a Python package for reading the Cube4 (.cubex) file format.
    Cube is used as a performance report explorer for Scalasca and Score-P.
    It is used as a generic tool for displaying a multi-dimensional
    performance space consisting of the dimensions (i) performance metric,
    (ii) call path, and (iii) system resource. Each dimension can be
    represented as a tree, where non-leaf nodes of the tree can be collapsed
    or expanded to achieve the desired level of granularity.
    The Cube4 (.cubex) data format is provided for Cube files produced with
    the Score-P performance instrumentation and measurement infrastructure or
    the Scalasca version 2.x trace analyzer (and other compatible tools)."""

    homepage = 'https://github.com/extra-p/pycubexr'
    pypi = 'pycubexr/pycubexr-1.2.0.tar.gz'

    maintainers = ['ageiss2', 'meaparvitas']

    version('1.2.0', sha256='5bf2c1b241f606480a2c918463af9007ea6d7f57c3723e021578ebccfcf52a13')

    depends_on('python@3.6:3', type=('build', 'run'))
