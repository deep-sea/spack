# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Psmgmt(AutotoolsPackage):
    """ParaStation Process Manager"""

    homepage = "https://par-tec.com"
    url = "https://github.com/ParaStation/psmgmt/archive/refs/tags/5.1.45-0.tar.gz"
    git = "https://github.com/ParaStation/psmgmt.git"

    maintainers = ["acastanedam"]

    version("master", branch="master")

    version("5.1.45-0", sha256="181b059ef438b1b329f50ae3a8f166b5dcee73bcdaad72a536655d2f723366fd")
    version("5.1.44-2", sha256="198ac07e55a28e3e09e2d6f2b303417de4c06bc5cf9a3ace3ce4a907e8610292")

    depends_on("autoconf", type="build")
    depends_on("automake", type="build")
    depends_on("libtool", type="build")
    depends_on("m4", type="build")
    depends_on("pkgconfig", type="build")

    depends_on("popt")
    depends_on("numactl")
    depends_on("hwloc")
    depends_on("munge")
    depends_on("linux-pam@1.4.0:")

    variant("spank", default=True, description="Slurm")
    variant("pspmix", default=True, description="Pmix")
    variant("testplugins", default=True, description="Testplugs")

    depends_on("slurm@:20-11-8-1", when="+spank")
    depends_on("slurm@:20-11-8-1", when="+testplugins")
    depends_on("pmix@3.1.3:", when="+pspmix")

    patch("0001-fix_config-force-relinking-of-pam-library.patch")
    patch("0001-fix_config-dynamically-set-path-of-sysctl.d.patch")

    def autoreconf(self, spec, prefix):
        autoreconf = which("autoreconf")
        autoreconf("-ifv")

    def configure_args(self):
        spec = self.spec
        # FIXME: psconfig is not part of open parastation
        args = ["--disable-psconfig"]
        args.extend(self.enable_or_disable("pspmix"))
        # requires Slurm for testplugins
        if "+spank" in spec or "+testplugins" in spec:
            args.extend(["--enable-spank"])
        args.extend(self.enable_or_disable("testplugins"))

        args += ["--with-pamdir={}/lib".format(spec["linux-pam"].prefix)]
        args += ["--with-unitdir={}/lib/systemd/system".format(spec.prefix)]

        return args

    # TODO: still not installing if tests fail
    @run_after("install")
    @on_package_attributes(run_tests=True)
    def test(self):
        make("check")
