# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack import *


class Mpcframework(AutotoolsPackage, CudaPackage):
    """The MPC (Multi-Processor Computing) framework provides a unified parallel
       runtime designed to improve the scalability and performances of applications
       running on clusters of (very) large multiprocessor/multicore NUMA nodes."""

    homepage = "https://mpc.hpcframework.com"
    url = "https://france.paratools.com/mpc/releases/mpcframework-4.0.0.tar.gz"

    maintainers = ['besnardjb']

    version('deepsea-v2',
            sha256='70aad6c77928b970f9ea74e23cfe1d978c7d991f6a446dcf8c987cc4adf95110',
            url='https://github.com/cea-hpc/mpc/archive/refs/tags/MPC_4.3.0.tar.gz'
    )
    version('4.1.0', sha256='ee20ca66a818d982d11e367595de2cf637b9eecf07b8dbc5484951995b13a382')
    version('4.0.1', sha256='8d65205b127a9c720cb1cebb9553afc709acc482c160b43ee0918f866d1bd75a')
    version(
        '4.0.0', sha256='14ba30a07c98e4de3f486685009105018059a3f65112cfe7f21a8f88c00bde21')

    # Optionnal dependencies
    variant('libunwind', default=True, description='Libunwind debug support')
    variant('cuda', default=False, description='Enable CUDA support')
    variant('fortran', default=True, description='Enable Fortran support')
    variant('mpcalloc', default=True,
            description='Enable MPC allocator support')
    variant('scheduler',
            values=disjoint_sets(
                ('auto',), ('hydra', 'slurm', 'pmi1')).with_non_feature_values('auto', 'none'),
            description='Launcher to be used')
    variant('priv', default=True, description='Compile MPC privatization support')

    # Optionnal Modules
    variant('openmp', default=True, description='Build MPC_OpenMP support')
    variant('lowcomm', default=True, description='Build MPC_Lowcomm support')
    variant('mpi', default=True, description='Build MPI support')
    variant('thread', default=True, description='Build MPC_Thread support')
    variant('io', default=True, description='Build MPI_IO support')

    # Optionnal features
    variant('profiler', default=False,
            description='Enable MPC profiling support')
    variant('color', default=True, description='Enable color output')
    variant('debug', default=False, description='Build debug support')
    variant('memdebug', default=False,
            description='Build memory debug support (ASAN)')
    variant('cma', default=False, description='Enable Cross Memory Attach')

    conflicts('+openmp', when="~thread")
    conflicts('+mpi', when="~thread")
    conflicts('+mpi', when="~lowcomm")
    conflicts('+io', when="~mpi")

    provides('mpi', when="+mpi")
    provides('mpi@:3.1', when="+mpi @4.0.0:")

    depends_on('hydra', when='scheduler=none')
    depends_on('hydra', when='scheduler=auto')
    depends_on('hydra', when='scheduler=hydra')

    depends_on('slurm', when='scheduler=slurm')
    depends_on('hwloc@:1', when="@4.0.0")
    depends_on('hwloc@2.2.0:', when="@4.0.1:")
    depends_on('hwloc@1: +cuda', when="+cuda @4.0.0")
    depends_on('hwloc@2: +cuda', when="+cuda @4.0.0:")
    depends_on('libunwind', when="+libunwind")
    depends_on('python', when="+fortran")
    depends_on('mpcalloc', when="+mpcalloc")
    depends_on('mpc-compiler-additions', when="+priv")
    depends_on('openpa')
    depends_on('libxml2')
    depends_on('autoconf@2.69', type='build')
    depends_on('automake@1.15', type='build')
    depends_on('libtool@2.4.6', type='build')

    # This is not ideal but currently the only
    # way to pass a "patched" compiler to a package
    def setup_build_environment(self, env):
        if self.spec.satisfies('+priv'):
            env.set('CC', 'apcc')
            env.set('CXX', 'ap++')
            env.set('FC', 'apfortran')

    @property
    def build_directory(self):
        return "spack-build-%s" % self.spec.dag_hash(7)

    def configure_args(self):
        spec = self.spec
        args = []

        # Launcher
        if spec.satisfies('scheduler=slurm'):
            args.append('--with-slurm={0}'.format(spec['slurm'].prefix))
        elif spec.satisfies('scheduler=pmi1'):
            if spec.satisfies('@4.1.0:'):
                # MPC 4.1.0 added support for forcing PMI1
                args.append('--with-pmi1')
            else:
                # With slurm alone may also look for PMIx
                args.append('--with-slurm')

        if spec.satisfies('-fortran'):
            args.append("--disable-fortran")

        if spec.satisfies('+cuda'):
            args.append('--with-cuda={0}'.format(spec['cuda'].prefix))
            args.append('--with-cuda-version={0}'.format(spec['cuda'].version))
            args.append('--with-cudart={0}'.format(spec['cuda'].prefix))
            args.append('--with-cudart-version={0}'.format(spec['cuda'].version))

        if spec.satisfies('+mpcalloc'):
            args.append('--with-mpcalloc={0}'.format(spec['mpcalloc'].prefix))

        if spec.satisfies('+profiler'):
            args.append('--enable-profiler')

        if spec.satisfies('+cma'):
            args.append('--enable-cma')

        if spec.satisfies('-priv'):
            args.append('--enable-process-mode')

        if spec.satisfies('+color'):
            args.append('--enable-shell-colors')

        if spec.satisfies('+debug'):
            args.append('--enable-debug')

        if spec.satisfies('+memdebug'):
            args.append('--enable-memory-debug')

        return args

    def setup_dependent_package(self, module, dependent_spec):
        self.spec.mpicc = join_path(self.prefix.bin, "mpicc")
        self.spec.mpicxx = join_path(self.prefix.bin, "mpicxx")
        self.spec.mpifc = join_path(self.prefix.bin, "mpif77")
        self.spec.mpif77 = join_path(self.prefix.bin, "mpif77")
        self.spec.mpicxx_shared_libs = [
            #join_path(self.prefix.lib, "libmpi_cxx.{0}".format(dso_suffix)),
            join_path(self.prefix.lib, "libmpcmpi.{0}".format(dso_suffix)),
        ]
