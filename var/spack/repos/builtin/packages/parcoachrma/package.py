# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
# Copyright 2022 Bull S.A.S - All rights reserved
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *
import os


class Parcoachrma(Package):
    """Analysis tool for MPI-RMA programs"""

    homepage = "https://parcoach.github.io/index.html"
    git      = "https://github.com/BullSequana/Parcoach-RMA"

    version('main', branch='main')
    version('deepsea-v2', branch='main')

    depends_on('classicflang@10.0.1')
    depends_on('classicclang@10.0.1')
    depends_on('openmpibull+classicclang-10')
    depends_on('cmake@3.24', type='build')

    def install(self, spec, prefix):
        os.environ['PREFIX'] = prefix
        make('install')
