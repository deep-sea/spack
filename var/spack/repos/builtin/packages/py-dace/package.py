# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyDace(PythonPackage):
    """DaCe is a parallel programming framework that takes code in Python/NumPy
and other programming languages, and maps it to high-performance CPU, GPU, and
FPGA programs, which can be optimized to achieve state-of-the-art. Internally,
DaCe uses the Stateful DataFlow multiGraph (SDFG) data-centric intermediate
representation: A transformable, interactive representation of code based on
data movement. Since the input code and the SDFG are separate, it is possible
to optimize a program without changing its source, so that it stays readable.
On the other hand, transformations are customizable and user-extensible, so
they can be written once and reused in many applications. With data-centric
parallel programming, we enable direct knowledge transfer of performance
optimization, regardless of the application or the target processor.  DaCe
generates high-performance programs for: Multi-core CPUs (tested on Intel, IBM
POWER9, and ARM with SVE), NVIDIA GPUs and AMD GPUs (with HIP), Xilinx and
Intel FPGAs. DaCe can be written inline in Python and transformed in the
command-line/Jupyter Notebooks or SDFGs can be interactively modified using the
Data-centric Interactive Optimization Development Environment Visual Studio
Code extension.
For more information, see our paper: http://www.arxiv.org/abs/1902.10345
"""

    homepage = "https://github.com/spcl/dace"
    pypi     = "dace/dace-0.14.tar.gz"
    version('0.14', sha256='a0db456def555e43c60706176e7d5544bafa71e2089695431f47acff2360da70')
    depends_on('py-setuptools', type='build')


