# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
# Copyright (c) 2022, Technical University of Darmstadt, Germany
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class Extrap(PythonPackage):
    """Extra-P: Automated performance modeling for HPC applications

    Extra-P is an automatic performance-modeling tool that supports the user in
    the identification of scalability bugs. A scalability bug is a part of the
    program whose scaling behavior is unintentionally poor, that is, much
    worse than expected. A performance model is a formula that expresses a
    performance metric of interest such as execution time or energy consumption
    as a function of one or more execution parameters such as the size of
    the input problem or the number of processors.

    Extra-P uses measurements of various performance metrics at different
    execution configurations as input to generate performance models of code
    regions (including their calling context) as a function of the execution
    parameters. All it takes to search for scalability issues even in full-blown
    codes is to run a manageable number of small-scale performance experiments,
    launch Extra-P, and compare the asymptotic or extrapolated performance of
    the worst instances to the expectations.

    Extra-P generates not only a list of potential scalability bugs but also
    human-readable models for all performance metrics available such as
    floating-point operations or bytes sent by MPI calls that can be further
    analyzed and compared to identify the root causes of scalability issues.

    Extra-P is developed by TU Darmstadt - in collaboration with ETH Zurich."""

    homepage = 'https://github.com/extra-p/extrap'
    pypi = 'extrap/extrap-4.0.4.tar.gz'




    maintainers = ['ageiss2', 'marcusritter1']
    version('deepsea-v2', 
            sha256='c250ae535fb5cb4f6006baf406029ce22dd3f9ce78008a8c48241347a4f90289',
            url="https://github.com/extra-p/extrap/archive/refs/tags/DEEP-SEA-V-2.tar.gz")
    version('deepsea-v1', 
            sha256='49f08dd830b8a943430e8e82b3cec28eabfac8980832193675ebf62fe28f4eb1',
            url="https://github.com/extra-p/extrap/archive/refs/tags/DEEP-SEA-V-1.tar.gz")
    version('4.3.0-alpha3', 
            sha256='91CBC84AFFE319B4DD7AEE3752E9944123759D0513FBA8A23C8418327727E7E5',
            url='https://github.com/extra-p/extrap/archive/refs/tags/v4.3.0-alpha3.tar.gz')
    version('4.0.4', sha256='c366feb74dfe938a5397ab5b939b3b1d88d85a3ca40afaad1017a5bbafd17590')

    variant('gui', default=False, description='Enable GUI')
    conflicts("+gui", when="@4.1:,deepsea-v1,deepsea-v2", msg="GUI currently not supported for versions above 4.1 using Spack because of missing dependencies.")

    depends_on('python@3.7:3', type=('build', 'run'), when='@:4.0')
    depends_on('python@3.8:3', type=('build', 'run'), when='@4.1:,deepsea-v1,deepsea-v2')
    
    depends_on('py-pyside2@5.13:5', type=('build', 'run'), when='@:4.0,deepsea-v1,deepsea-v2 +gui')
    #depends_on('py-pyside6@6.4:6', type=('build', 'run'), when='@4.1:,deepsea-v1,deepsea-v2 +gui')    
    depends_on('py-matplotlib@3.2:3', type=('build', 'run'), when='@:4.0,deepsea-v1 +gui')
    depends_on('py-matplotlib@3.5:3', type=('build', 'run'), when='@4.1:,deepsea-v1,deepsea-v2 +gui')
    depends_on('py-numpy@1.18:1', type=('build', 'run'))
    depends_on('py-tqdm@4.47:4', type=('build', 'run'))
    depends_on('py-pycubexr@1.1:1', type=('build', 'run'))
    depends_on('py-marshmallow@3.7:3', type=('build', 'run'))
    depends_on('py-packaging@20.0:', type=('build', 'run'))
    depends_on('py-kaitaistruct@0.9:0', type=('build', 'run'), when='@4.3:,deepsea-v1,deepsea-v2')
    depends_on('py-protobuf@3.14:3', type=('build', 'run'), when='@4.3:,deepsea-v1,deepsea-v2')
    depends_on('py-itanium-demangler@1.0:1', type=('build', 'run'), when='@4.3:,deepsea-v1,deepsea-v2')
    depends_on('py-sympy@1.8:1', type=('build', 'run'), when='@4.3:,deepsea-v1,deepsea-v2')
    depends_on('py-typing-extensions@4.3:4', type=('build', 'run'), when='@4.3:,deepsea-v1,deepsea-v2')
    depends_on('py-msgpack@1.0:1', type=('build', 'run'), when='@4.3:,deepsea-v1,deepsea-v2')
    depends_on('py-scipy@1.5:1', type=('build', 'run'), when='@4.3:,deepsea-v1,deepsea-v2')
    #depends_on('py-pyvista@0.42:0', type=('build', 'run'), when='@4.3:,deepsea-v2 +gui')
    #depends_on('py-pyvistaqt@0.11:0', type=('build', 'run'), when='@4.3:,deepsea-v2 +gui')
