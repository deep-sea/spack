# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class OmpssMusa(BundlePackage):
    """
    OmpSs is an effort to integrate features from the StarSs programming model 
    developed at BSC into a single programming model. In particular, our 
    objective is to extend OpenMP with new directives to support asynchronous 
    parallelism and heterogeneity (devices like GPUs, FPGAs). However, it can 
    also be understood as new directives extending other accelerator-based APIs 
    like CUDA or OpenCL. Our OmpSs environment is built on top of our Mercurium 
    compiler and Nanos++ runtime system.
    """

    homepage = "https://pm.bsc.es"

    version('deepsea-v2')

    depends_on('nanox')
    depends_on('mcxx')
