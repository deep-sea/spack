# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import shutil
import os


class Dynamorio(Package):
        """
        DynamoRIO is a runtime code manipulation system that supports code transformations 
        on any part of a program, while it executes. DynamoRIO exports an interface for building 
        dynamic tools for a wide variety of uses: program analysis and understanding, profiling, 
        instrumentation, optimization, translation, etc. Unlike many dynamic tool systems, 
        DynamoRIO is not limited to insertion of callouts/trampolines and allows arbitrary 
        modifications to application instructions via a powerful IA-32/AMD64/ARM/AArch64 instruction 
        manipulation library. DynamoRIO provides efficient, transparent, and comprehensive 
        manipulation of unmodified applications running on stock operating systems (Windows, Linux, 
        or Android, with experimental Mac support) and commodity IA-32, AMD64, ARM, and AArch64 hardware.
        """
        
        homepage = "https://dynamorio.org/"
        url      = "https://github.com/DynamoRIO/dynamorio/releases/download/release_9.0.1/DynamoRIO-Linux-9.0.1.tar.gz"

        version('9.0.1', sha256='90452768bda1ee9f123f86a916ecb0a442c59bade36d999de73f759c4f79e6e7')
        
        
        def install(self, spec, prefix):

            files_list = os.listdir(os.getcwd())
            for file in files_list:
                print(file)
                shutil.move(file, prefix)
