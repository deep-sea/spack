# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import shutil

class Nanox(AutotoolsPackage):
    """
    Nanos++ is a runtime designed to serve as runtime support in parallel 
    environments. It is mainly used to support OmpSs, an extension to OpenMP 
    developed at BSC. It also has modules to support OpenMP.
    """

    homepage = "https://pm.bsc.es/nanox"
    url = "https://github.com/dchasap/MUSA/releases/download/deepsea/nanox-tasksim-nov2020.tar.gz"

    version('deepsea-v2', sha256='0af76fe7cbe5678dbaddd705b89ce9b2f815d596b05d064b05e053adc5ce886f')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type='build')

    depends_on('papi', type=('build', 'run'))
    depends_on('extrae', type=('build', 'run'))
    depends_on('tasksim-base', type=('build', 'run'))



    def configure_args(self):

        args = ['--with-papi={0}'.format(self.spec['papi'].prefix),
                '--with-extrae={0}'.format(self.spec['extrae'].prefix),
                '--with-nextsim={0}'.format(self.spec['tasksim-base'].prefix),
                '--disable-allocator',
                '--disable-debug']
        return args
    

    def install(self, spec, prefix):

        make('install')
        shutil.move("./src", prefix)
