# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Tasksim(AutotoolsPackage):
    """
    TaskSim is a trace driven architectural simulator that can simulate high 
    level OpenMP/OmpSs runtime events as well as low level instructions.
    """

    homepage = "https://www.bsc.es/research-and-development/publications/musa-multi-level-simulation-approach-next-generation-hpc"
    url      = "https://github.com/dchasap/MUSA/releases/download/deepsea/tasksim-3.1_profet.tar.gz"

    version('deepsea-v2', sha256='b9ad629b5c709571547595a205583696746c30c24d1189f222596e761a55038f')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type='build')

    depends_on('extrae', type=('build', 'run'))
    depends_on('dynamorio', type=('build', 'run'))
    depends_on('ompss-musa', type=('build', 'run'))
    depends_on('openmpi', type=('build', 'run'))
    depends_on('ramulator', type=('build', 'run'))

 
    def autoreconf(self, spec, prefix):
        autoreconf('--install', '--verbose', '--force')



    def configure_args(self):
        args = ['--enable-nanox', 
                '--with-nanox-src-path={0}'.format(self.spec['nanox'].prefix),
                '--with-nanox-install-path={0}'.format(self.spec['nanox'].prefix),
                '--enable-paraver',
                '--enable-ptlsim',
                '--enable-mpi',
                '--with-mpi-install-path={0}'.format(self.spec['openmpi'].prefix),
                '--with-dr-path={0}'.format(self.spec['dynamorio'].prefix),
                '--enable-ramulator',
                '--with-ramulator-src-path={0}'.format(self.spec['ramulator-musa'].prefix),
                '--with-ramulator-lib-path={0}'.format(self.spec['ramulator-musa'].prefix),
                '--enable-compressed-traces',
                '--with-only-tasksim',
                '--enable-timestats',
                'traceformat=rle']    

        return args
