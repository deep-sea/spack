# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class Wxparaver(AutotoolsPackage):
    """"A very powerful performance visualization and analysis tool
        based on traces that can be used to analyse any information that
        is expressed on its input trace format.  Traces for parallel MPI,
        OpenMP and other programs can be genereated with Extrae."""
    homepage = "https://tools.bsc.es/paraver"
    url = "https://ftp.tools.bsc.es/wxparaver/wxparaver-4.10.4-src.tar.bz2"

    version('deepsea-v2', sha256='86103a6c4a6579a88dfd6141d5a8a17d924225d45e428a2165c044f4c7555670')
    version('4.11.2', sha256='86103a6c4a6579a88dfd6141d5a8a17d924225d45e428a2165c044f4c7555670')
    version('4.11.1', sha256='b1c1cc8ce69adb095b9f8a4500250c0baa50668be60825f75d75fddca9a88f77')
    version('4.11.0', sha256='2c9e3ec717412aa0bd32949aa9ab93bacbac140fc2020b20209a27cf35ed768e')
    version('4.10.6', sha256='3c6dfb78de6a3a59435e27481da5148a1c1cdb608016c33147cc17938ea77ece')
    version('4.10.5', sha256='994b37a7064bbf74837503eb7be08be6b43ba74fdaed0c274b0ee58e33762929')
    version('4.10.4', sha256='b5609c6918154e6f67a3484debddd9f4a0ce7f83be5ab6e889d9d7e5d4333012')

    depends_on('boost@1.79: +serialization +date_time')
    depends_on('wxwidgets@3.1.0:')  # NOTE: using external for this one is usually simpler
    depends_on('openssl@1.1.1:')
    depends_on('libxml2')
    depends_on('zlib')

    def configure_args(self):
        spec = self.spec
        args = []

        args.append('--with-boost=%s' % spec['boost'].prefix)
        args.append('--with-wx-config=%s/wx-config' % spec['wxwidgets'].prefix.bin)
        args.append('--with-openssl=%s' % spec['openssl'].prefix)
        args.append('--with-xml-prefix=%s' % spec['libxml2'].prefix)
        args.append('--enable-openmp')
        args.append('--with-debug-level=release')
        args.append('CXXFLAGS=-std=c++14')

        return args

    # use make install directly as expected by Paraver (See README)
    def build(self, spec, prefix):
        pass

