# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Mcxx(AutotoolsPackage):
    """
    Mercurium is a source-to-source compilation infrastructure aimed 
    at fast prototyping. Current supported languages are C, C++ and 
    Fortran. Mercurium is mainly used in Nanos environment to implement 
    OpenMP but since it is quite extensible it has been used to implement 
    other programming models or compiler transformations, examples include 
    Cell Superscalar, Software Transactional Memory, Distributed Shared 
    Memory or the ACOTES project, just to name a few.
    """

    homepage = "https://pm.bsc.es/mcxx"
    url = "https://github.com/dchasap/MUSA/releases/download/deepsea/mcxx-2.3.0.tar.gz"

    version('deepsea-v2', sha256='49caed5566e0d15b2b5b26b05a5e9c7471363736422cb66f4510780f4b497234')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type='build')

    depends_on('sqlite')
    depends_on('nanox')


    def autoreconf(self, spec, prefix):
        autoreconf('--install', '--verbose', '--force')


    def configure_args(self):
        args = ['--enable-ompss',
                '--with-nanox={0}'.format(self.spec['nanox'].prefix),
                'LIBS=-liconv'] 

        return args
