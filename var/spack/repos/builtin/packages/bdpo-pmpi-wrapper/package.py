# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class BdpoPmpiWrapperPackage(MakefilePackage):
    homepage = "https://eviden.com"
    maintainers = ["Mathieu Stoffel", "Tony Ranini"]

    def url_for_version(self, version):
        deepsea_releases = {"deepsea-v2": "4.3.2"}

        if version in deepsea_releases:
            version = deepsea_releases[version]

        return f"file:///home/di93nad/bdpo-pmpiwrapper-buildpack-{version}.tar.gz"

    version(
        "deepsea-v2",
        sha256="b3eceee09afad75c628c324081a2e443b1ef13dcb57a81b3db5a1099eb02e83b",
        preferred=True,
    )
    version("4.3.2", sha256="b3eceee09afad75c628c324081a2e443b1ef13dcb57a81b3db5a1099eb02e83b")

    def bdpo_get_mpi_env(self, spec):
        pass

    # First step, setup the environment
    def edit(self, spec, prefix):
        mpi = self.bdpo_get_mpi_env(spec)
        env["MPICC"] = mpi["mpicc"]
        env["MPICH"] = mpi["mpich_enabled"]
        env["DPO_PMPI_MPI_FLAVOR"] = str(mpi["mpi_name"])
        env["DPO_PMPI_MPI_VERSION"] = str(mpi["mpi_version"])
        env["INSTALL_OPT_DIR"] = prefix.opt
        env["INSTALL_LIB_DIR"] = ""

    def setup_run_environment(self, env):
        mpi = self.bdpo_get_mpi_env(self.spec)
        env.set(
            "BDPOPMPIWRAPPER",
            f"{self.prefix.opt}/bdpopmpiwrapper-{mpi['mpi_name']}-{mpi['mpi_version']}.so",
        )


class BdpoPmpiWrapper(BdpoPmpiWrapperPackage):
    """This package provides the
    Power Efficiency Dynamic Power Optimizer PMPI Wrapper product.
    WARNING: You can't install this package, see bdpo-pmpi-wrapper-*
    """

    def bdpo_get_mpi_env(self, spec):
        raise InstallError(
            "Can't install bdpo-pmpi-wrapper without an mpi provider. See bdpo-pmpi-wrapper-*"
        )
