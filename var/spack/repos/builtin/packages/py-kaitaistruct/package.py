# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyKaitaistruct(PythonPackage):
    """Kaitai Struct is a declarative language used for describing various
    binary data structures laid out in files or in memory:
    i.e., binary file formats, network stream packet formats, etc.

    The main idea is that a particular format is described in Kaitai Struct
    language only once and then can be compiled with ksc into source files
    in one of the supported programming languages. These modules will include
    a generated code for a parser that can read described data structure from
    a file/stream and give access to it in a nice, easy-to-comprehend API."""

    homepage = 'https://kaitai.io/'
    pypi = 'kaitaistruct/kaitaistruct-0.10.tar.gz'

    version('0.10', sha256='a044dee29173d6afbacf27bcac39daf89b654dd418cfa009ab82d9178a9ae52a')

    depends_on('python@2.7:2,3.4:3', type=('build', 'run'))
