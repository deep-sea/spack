# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import os
import shutil

class TasksimBase(Package):
    """
    TaskSim is a trace driven architectural simulator that can 
    simulate high level OpenMP/OmpSs runtime events as well as 
    low level instructions (This only install the source files
    required by Nanos++ for instrumentation).
    """

    homepage = 'https://www.bsc.es/research-and-development/publications/musa-multi-level-simulation-approach-next-generation-hpc'
    url      = "https://github.com/dchasap/MUSA/releases/download/deepsea/tasksim-3.1_profet.tar.gz"

    version('3.1', sha256='b9ad629b5c709571547595a205583696746c30c24d1189f222596e761a55038f')

    def install(self, spec, prefix):
        #source = os.path.join(, "src")
        src_dir = os.getcwd() + "/src"
        dest_dir = prefix + "/include/nextsim"
        os.makedirs(dest_dir)
        files_list = os.listdir(src_dir)
        for file in files_list:
            shutil.move("src/" + file, dest_dir)
