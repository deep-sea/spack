# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Memaxes(Package):
    """MemAxes is a visualizer for sampled memory trace data."""

    homepage = "https://github.com/caps-tum/MemAxes"
    url = "https://github.com/caps-tum/MemAxes/archive/refs/tags/v0.6.1.tar.gz"
    git = "https://github.com/caps-tum/MemAxes"

    version('deepsea-v2', sha256='a537483d13849b2b64f6357cad16ad2ce7ab10c1a873ee67bb50b55e30751711')
    version('0.7.0', sha256='a537483d13849b2b64f6357cad16ad2ce7ab10c1a873ee67bb50b55e30751711')
    version('master', branch='master')


    depends_on('cmake@3.21:', type='build')
    depends_on("sys-sage@0.4.2:~build_data_sources")
    depends_on("qt+opengl@5.15.2:")

    def install(self, spec, prefix):
        with working_dir("spack-build", create=True):
            cmake("..", *std_cmake_args)
            make()
            make("install")
