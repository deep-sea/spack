# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack import *


def _gcc_patch_variants():
    return {"workshare", "autopriv", }


def _gcc_version():
    return {"4.8.5", "6.2.0", "7.4.0", "7.3.0", "7.5.0", "8.1.0", "8.2.0", "8.3.0", "8.4.0", "9.1.0", "9.2.0", "9.3.0", "10.1.0", "10.3.0", "10.2.0", "11.1.0", "11.2.0", "11.3.0", "12.1.0", "12.2.0", }


class MpcCompilerAdditions(AutotoolsPackage):
    """Patched compiler and its driver for the MPC runtime. This includes
    extended TLS support to convert programs to user-level threads and
    workshare extensions."""

    homepage = "http://mpc.hpcframework.com"
    url = "https://france.paratools.com/mpc/releases/mpc-compiler-additions-0.6.2.tar.gz"
    version('0.9.1',
            sha256='6c53d22d9af4070130c959366a72a0e8efcdaa9baf530d7f6c8fb17369bcc6c7')
    version('0.9.0',
            sha256='6674cd3d308f98b5b7bfe21109ed26ccac8ac4520c1adc7ca2c91eeab9f159f8')
    version('0.8.0',
            sha256='add07408b206468afc9288aff45b412b86a039b5da3516fc3c05b7649256e511')
    version('0.7.0',
            sha256='e7201d8837c6cc7426a7accb8f25c8e27c17e4978c8a8d7251e4050bc452aa55')
    version('0.6.3',
            sha256='61e2e6f7a15642190c6e757ad456cfbef522938a7dae37f0e2997ef50808d5e9')

    depends_on("hwloc@2.7.0")
    depends_on("openpa")
    depends_on("libelf", when="+libelf")

    variant("debug", default=False, description="Enable debug mode")
    variant("libelf", default=True, description="Use libelf for symbol introspection")
    variant("ccpatch", default=True,
            description="Install and deploy embedded GNU GCC privatizing compiler")
    variant("gcc_version", default="10.2.0",
            description='GCC version to be deployed',
            values=_gcc_version())

    variant("autopriv", default=True, description="Enable autopriv support")
    variant("workshare", default=True, description="Enable workshare support")

    conflicts("+workshare",
              when="gcc_version=12.2.0",
              msg="gcc_version=12.2.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=12.1.0",
              msg="gcc_version=12.1.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=11.3.0",
              msg="gcc_version=11.3.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=11.2.0",
              msg="gcc_version=11.2.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=11.1.0",
              msg="gcc_version=11.1.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=10.2.0",
              msg="gcc_version=10.2.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=10.3.0",
              msg="gcc_version=10.3.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=10.1.0",
              msg="gcc_version=10.1.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=9.3.0",
              msg="gcc_version=9.3.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=9.2.0",
              msg="gcc_version=9.2.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=9.1.0",
              msg="gcc_version=9.1.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=8.4.0",
              msg="gcc_version=8.4.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=8.3.0",
              msg="gcc_version=8.3.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=8.2.0",
              msg="gcc_version=8.2.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=8.1.0",
              msg="gcc_version=8.1.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=7.5.0",
              msg="gcc_version=7.5.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=7.4.0",
              msg="gcc_version=7.4.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=6.2.0",
              msg="gcc_version=6.2.0 is not compatible with variant workshare")
    conflicts("+workshare",
              when="gcc_version=4.8.5",
              msg="gcc_version=4.8.5 is not compatible with variant workshare")

    def configure_args(self):
        spec = self.spec

        libelfadd=""
        if spec.satisfies("+libelf"):
            libelfadd="--with-libelf={}".format(spec['libelf'].prefix)

        options = [
            '--opts="--with-openpa={0} --with-hwloc={1} {2}"'.format(
                spec['openpa'].prefix, spec['hwloc'].prefix, libelfadd)
        ]

        if spec.satisfies("+debug"):
            options.extend(['--enable-debug'])
        if spec.satisfies("-ccpatch"):
            options.extend(['--disable-gcc'])
        else:
            options.extend(['--gcc-version={}'.format(spec.variants['gcc_version'].value)])

        
        if spec.satisfies("-autopriv"):
            options.extend(['--disable-autopriv'])
        if spec.satisfies("-workshare"):
            options.extend(['--disable-workshare'])

        return options
