# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *
import os

class Openmpibull(AutotoolsPackage):
    """An open source Message Passing Interface implementation.

    The Open MPI Project is an open source Message Passing Interface
    implementation that is developed and maintained by a consortium
    of academic, research, and industry partners. Open MPI is
    therefore able to combine the expertise, technologies, and
    resources from all across the High Performance Computing
    community in order to build the best MPI library available.
    Open MPI offers advantages for system and software vendors,
    application developers and computer science researchers.
    """

    homepage = "https://www.open-mpi.org"
    git = "https://github.com/BullSequana/OpenMPI5"

    version('5.0.2', branch='v5.0.2_Bull', submodules=True)

    variant('classicclang-10', default=False, description="Compile OpenMPI-Bull with classicclang-10 (for ParcoachRMA)")
    variant('classicclang-15', default=False, description="Compile OpenMPI-Bull with classicclang-15 (for Parcoach)")

    depends_on("autoconf", type="build")
    depends_on("automake", type="build")
    depends_on("libtool", type="build")

    depends_on("classicclang@10.0.1", when="+classicclang-10")
    depends_on("classicflang@10.0.1", when="+classicclang-10")

    depends_on("classicclang@15.0.3", when="+classicclang-15")
    depends_on("classicflang@15.0.3", when="+classicclang-15")

    def setup_build_environment(self, env):
        if "+classicclang-15" in self.spec or "+classicclang-10" in self.spec:
            env.set('CC', 'clang')
            env.set('CXX', 'clang++')
            env.set('FC', 'flang')

    def configure_args(self):
        args = []

        args.append('--enable-mpi-ext=notified_rma')
        args.append('--with-pmix=internal')
        args.append('--enable-mpirun-prefix-by-default')
        args.append('--with-cma')
        args.append('--with-lustre=no')
        args.append('--with-verbs=no')
        args.append('--with-hcoll=no')
        args.append('--with-libevent=internal')
        args.append('--with-hwloc=internal')
        args.append('--without-cuda')
        args.append('--enable-mem-debug=no')
        args.append('--enable-mem-profile=no')
        args.append('--enable-debug=no')
        args.append('--enable-debug-symbols=no')
        args.append('--enable-memchecker=no')
        args.append('--enable-orterun-prefix-by-default=yes')
        args.append('--enable-wrapper-rpath=no')
        args.append('--enable-wrapper-runpath=no')
        args.append('--enable-mca-no-build=crs,snapc,pml-crcpw,pml-v,vprotocol,crcp,btl-usnic,btl-uct,btl-openib,coll-ml')
        args.append('--enable-io-romio=yes')
        args.append('--with-io-romio-flags=--with-file-system=ufs+nfs')

        return args

    def autoreconf(self, spec, prefix):
        os.system('./autogen.pl')
