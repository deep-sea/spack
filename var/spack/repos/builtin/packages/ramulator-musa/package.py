# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class RamulatorMusa(MakefilePackage):
    """
    Ramulator is a fast and cycle-accurate DRAM simulator that supports
    a wide array of commercial, as well as academic, DRAM standards.
    """

    homepage = "https://github.com/CMU-SAFARI/ramulator"
    url = 'file:///p/project/deepsea/wp2/tool_sources/musa/ramulator-musa/ramulator.tar.gz' 
#    version('sst-musa', sha256='4391ecc2ad9906c7aada234d021111634d3a43ef623d7eac464da0830818fed6',
#            url='file:///p/project/deepsea/wp2/tool_sources/ramulator-musa/ramulator.tar.gz')
 
    version('sst-musa', sha256='ee7b4373fdb22173f90d717199287dba39b935e6c2f9f2bdd2b21101fc8ce79a')

#    maintainers = ['chasapis2']

    def build(self, spec, prefix):
        make("libramulator.a")

    def install(self, spec, prefix):
        install_tree(".", prefix)
