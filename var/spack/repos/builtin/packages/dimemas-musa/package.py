# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class DimemasMusa(AutotoolsPackage):
    """High-abstracted network simulator for message-passing programs."""

    homepage = "https://tools.bsc.es/dimemas"
    url      = "https://github.com/dchasap/MUSA/releases/download/deepsea/dimemas-5.2.12_musa.tar.gz"	

    version('deepsea-v2', sha256='93b7bda482f8fe2fb32fa418a1da59d6a821dc216cf5a4cd58349d0151d02601')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type='build')

    depends_on('bison', type=('build', 'link', 'run'))
    depends_on('flex', type=('build', 'link', 'run'))
#    depends_on('boost@1.65.0+container+math+exception+program_options cxxstd=11', type=('build', 'link'))

    def autoreconf(self, spec, prefix):
        autoreconf('--install', '--verbose', '--force')

    def configure_args(self):
#        args = ["--with-boost=%s" % self.spec['boost'].prefix,
#                "--with-boost-libdir=%s" % self.spec['boost'].prefix.lib,
        args = ["LEXLIB=-l:libfl.a"]

        return args


