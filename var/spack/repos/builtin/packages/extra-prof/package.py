# Copyright 2013-2023 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from pathlib import Path
from spack.package import *

import glob
import os
import shutil
import stat


class ExtraProf(Package):
    """Extra-Prof is a profiler for combined profiling of CPU and GPU
    performance. It supports capturing the concurrent execution of
    activities (kernels and memory transfers) on the GPU and integrates them
    with the CPU call tree to provide a unified call tree for heterogeneous
    applications."""

    homepage = "https://github.com/extra-p/extrap"
    url = "https://github.com/extra-p/extrap"

    maintainers("ageiss2")

    version('deepsea-v2', 
            sha256='c250ae535fb5cb4f6006baf406029ce22dd3f9ce78008a8c48241347a4f90289',
            url="https://github.com/extra-p/extrap/archive/refs/tags/DEEP-SEA-V-2.tar.gz")
    version('deepsea-v1', 
            sha256='49f08dd830b8a943430e8e82b3cec28eabfac8980832193675ebf62fe28f4eb1',
            url="https://github.com/extra-p/extrap/archive/refs/tags/DEEP-SEA-V-1.tar.gz")

    depends_on("cuda@11.7:12", type=("build", "run"))

    def install(self, spec, prefix):
        extr_prof_path = Path(self.stage.source_path) / "tools" / "extra-prof"

        wrappers = glob.glob("*.sh", root_dir=extr_prof_path)
        for w in wrappers:
            w_file = extr_prof_path / w
            mode = os.stat(w_file)
            os.chmod(w_file, mode.st_mode | stat.S_IEXEC)

        os.system(
            'EXTRA_PROF_COMPILER="echo" '
            + str(extr_prof_path / "extra-prof-wrapper.sh")
        )

        os.remove(extr_prof_path / "msgpack-cxx.tar.gz")

        shutil.copytree(extr_prof_path, Path(prefix) / "bin", dirs_exist_ok=True)
