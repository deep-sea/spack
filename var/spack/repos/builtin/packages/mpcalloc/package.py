# Copyright 2013-2023 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack import *


class Mpcalloc(CMakePackage):
    """mpcalloc is a thread-aware and NUMA-aware memory allocator (malloc, calloc, realloc, free, memalign and posix_memalign). It implements a per-thread heap to avoid contention during allocation and to maintain data locality on NUMA nodes."""

    homepage = "http://mpc.hpcframework.com"
    url      = "https://france.paratools.com/mpcalloc-0.1.tar.gz"

    version('deepsea-v2',
            sha256='3c94ba0b83361da48ba95283e265c76fbf182094fdb03c594c62397849e0627c',
            url='https://github.com/cea-hpc/mpc-allocator/archive/72e3ced3454c6a9e3fbf8934b3cf369631a90958.zip'
    )
    version('0.2', sha256='a4a8f05d53c0c80a90020fd4043bcf807521996416aa22ed61a8e1fcf1bedc62')
    version('0.1', sha256='b442405a4a069e57ca9d47e2cd447b28a979a4cd63ee9680f5e44ac54cdb02cb')

    variant("memcheck", default=False, description="Enable memcheck integration")

    depends_on('openpa')
    depends_on('hwloc@:1.99.99', when="@0.1")
    depends_on('hwloc', when="@0.2:")
    depends_on('valgrind ~mpi', when="+memcheck")

