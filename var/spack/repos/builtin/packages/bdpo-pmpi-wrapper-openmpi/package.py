# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# Workaround for imported Python module with dashes
import importlib

from spack.package import *

bdpopmpiwrapper = importlib.import_module("spack.pkg.builtin.bdpo-pmpi-wrapper")


class BdpoPmpiWrapperOpenmpi(bdpopmpiwrapper.BdpoPmpiWrapperPackage):
    """This package provides the
    Power Efficiency Dynamic Power Optimizer PMPI Wrapper product for OpenMPI
    """

    depends_on("openmpi")

    def bdpo_get_mpi_env(self, spec):
        return {
            "mpicc": spec["mpi"].mpicc,
            "mpich_enabled": "0",
            "mpi_name": spec["mpi"].name,
            "mpi_version": spec["mpi"].version,
        }
