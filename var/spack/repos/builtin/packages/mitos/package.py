# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Mitos(CMakePackage):
    """Mitos is a library and a tool for collecting sampled memory
    performance data to view with MemAxes"""

    homepage = "https://github.com/caps-tum/mitos"
    url      = "https://github.com/caps-tum/mitos/archive/refs/tags/v1.0.0.tar.gz"
    git      = "https://github.com/caps-tum/mitos.git"

    version('deepsea-v2', url='https://github.com/caps-tum/mitos/archive/refs/tags/v1.0.1.tar.gz', sha256='72329c75e0b0f5ada255ef9984fac8f2ae9700e358503ca0e5bde6547e1cd234')
    version('1.0.1', url='https://github.com/caps-tum/mitos/archive/refs/tags/v1.0.1.tar.gz', sha256='72329c75e0b0f5ada255ef9984fac8f2ae9700e358503ca0e5bde6547e1cd234')
    version('1.0.0', url='https://github.com/caps-tum/mitos/archive/refs/tags/v1.0.0.tar.gz', sha256='1ad0307851208121fa6ec212b02fa09436057b600e043fbe72914243613f6980')
    version('0.10.1', sha256='aa0423df10a2d1db45f52c2297ed5814112542aa315e6e9dcb3d49a4d8c4f463')
    version('0.10.1-dyninst-optional', sha256='6a80e691b353847a2ac030ddb58f4fdb57543bf18061a4555116d36b0df5b128')
    version('master', branch='master')
    version('develop', branch='develop')

    depends_on('dyninst@12.2.0:')
    depends_on('hwloc@2.9:')
    depends_on('mpi')
    depends_on('cmake@3.21:', type='build')

