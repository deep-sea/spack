# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Musa(BundlePackage):
    """
    MUSA is simulation methodology that combines different levels of abstraction 
    over an entire HPC system.  It can generate Paraver traces, which enables 
    users to analyse their applications on a simulated environment.
    """

    homepage = 'https://www.bsc.es/research-and-development/publications/musa-multi-level-simulation-approach-next-generation-hpc'

    maintainters = ['dchasapi@bsc.es']

    version('deepsea-v2')

    depends_on('openmpi')
    depends_on('ompss-musa')
    depends_on('dimemas-musa')
    depends_on('tasksim')
