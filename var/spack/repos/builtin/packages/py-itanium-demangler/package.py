# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyItaniumDemangler(PythonPackage):
    """The Python Itanium Demangler is a pure Python parser for the Itanium C++ ABI
    symbol mangling language. Note that MSVC mangling language is not supported.
    This demangler generates an abstract syntax tree from mangled symbols, which can
    be used for directly extracting type information, as opposed to having to
    interpret the C++ source code corresponding to the demangled symbol"""

    homepage = 'https://github.com/whitequark/python-itanium_demangler'
    pypi = 'itanium_demangler/itanium_demangler-1.0.tar.gz'
    url = 'https://github.com/whitequark/python-itanium_demangler/archive/refs/tags/v1.1.tar.gz'

    version('1.1', sha256='b919af8c229247824ad8e0f8f670855b23d9b816560befb122d70ee59b062e89')
    version('1.0', sha256='bca0fae4cb0ad582a3c7ede97d66b325d13445fdf87c28947233d8559430967c')
