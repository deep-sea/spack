. share/spack/setup-env.sh
spack env activate --without-view .
spack buildcache keys --install --trust
spack mirror list
spack clean -a -b
spack env deactivate
spack install gcc@11.3.0
spack load gcc@11.3.0
spack compiler find
spack unload gcc@11.3.0
spack env activate --without-view .
spack --env . install
